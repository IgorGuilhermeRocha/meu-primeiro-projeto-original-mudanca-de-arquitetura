import java.util.Scanner;
import java.io.*;

public class Program {

	public static void main(String[] args) {
		Scanner in = new Scanner(System.in);
		Scanner is = new Scanner(System.in);

		String vetNome[] = new String[100];
		long vetCpf[] = new long[100];
		int vetIdade[] = new int[100];
		String vetEndereco[] = new String[100];
		String vetConvenio[] = new String[100];
		long vetOldCpf[] = new long[100];
		Long vetConsultas[] = new Long[100];

		PrintWriter arqRelatorio = null;

		int op;
		int p = 0;

		String vetSemana[][] = new String[5][8];

		String vetDia[] = { "Segunda", "Terca", "Quarta", "Quinta", "Sexta" };
		String vetH[] = { "8", "9", "10", "11", "13", "14", "15", "16" };

		System.out.println("------------------------------------");
		System.out.println("  BEM VINDO AO NOSSO CONSULTORIO ");
		System.out.println("------------------------------------");

		do {
			System.out.println("1- Cadastrar pacientes;");
			System.out.println("2- Editar informacoes de pacientes;");
			System.out.println("3- Buscar pacientes;");
			System.out.println("4- Marcar consulta para um paciente;");
			System.out.println("5- Relatorio semanal;");
			System.out.println("6- Sair");
			System.out.println("-------------------------------------");

			System.out.print("Agora escolha umas das opcoes: ");
			op = in.nextInt();
			System.out.println("-------------------------------------");

			switch (op) {
				case 1:
					if (p < 100) {
						boolean teste;

						System.out.println("Paciente: ");
						System.out.print("Nome: ");
						String nome = is.nextLine();

						long cpf;

						do {
							teste = true;
							System.out.print("Cpf: ");
							cpf = Math.abs(in.nextLong());
							if (p != 0) {
								for (int c = 0; c < p; c++) {
									if (vetOldCpf[c] == cpf) {
										teste = false;
										break;
									}

								}

							}

							if (teste == false) {
								System.out.println("Cpf invalido...");
								System.out.println("-------------------------------------");
							}

						} while (teste == false);

						vetOldCpf[p] = cpf;
						int idade;

						do {
							System.out.print("Idade: ");
							idade = in.nextInt();
							if (idade < 0) {
								System.out.println("Idade invalida...");
								System.out.println("-------------------------------------");
							}
						} while (idade < 0);

						System.out.print("Endereco: ");
						String endereco = is.nextLine();

						System.out.print("Convenio: ");
						String convenio = is.nextLine();

						vetNome[p] = nome;
						vetCpf[p] = cpf;
						vetIdade[p] = idade;
						vetEndereco[p] = endereco;
						vetConvenio[p] = convenio;

						p++;

						System.out.println("-------------------------------------");

					} else {
						System.out.println("Numero maximo de pacientes atingidos...");
						System.out.println("-------------------------------------");
					}
					break;

				case 2:
					if (p != 0) {
						boolean teste = false;
						int indice = -1;
						int op2;

						System.out.print("Digite o cpf do paciente: ");
						long cpf = in.nextLong();
						System.out.println("-------------------------------------");

						for (int c = 0; c < p; c++) {
							if (vetCpf[c] == cpf) {
								indice = c;
								teste = true;
								break;
							}
						}
						if (teste == true) {
							do {
								System.out.println("1- Nome;     //Atual: " + vetNome[indice]);
								System.out.println("2- Idade;    //Atual: " + vetIdade[indice]);
								System.out.println("3- Endereco; //Atual: " + vetEndereco[indice]);
								System.out.println("4- Convenio; //Atual: " + vetConvenio[indice]);
								System.out.println("5- Sair;");
								System.out.println("-------------------------------------");
								System.out.print("Escolha uma das opcoes: ");
								op2 = in.nextInt();
								System.out.println("-------------------------------------");
								switch (op2) {
									case 1:
										System.out.print("Digite um novo nome: ");
										String nome = is.nextLine();
										vetNome[indice] = nome;
										System.out.println("-------------------------------------");
										System.out.println("Troca de dados completa!!!");
										System.out.println("-------------------------------------");
										break;
									case 2:
										int idade;
										do {
											System.out.print("Digite uma nova idade: ");
											idade = in.nextInt();
										} while (idade < 0);
										vetIdade[indice] = idade;
										System.out.println("-------------------------------------");
										System.out.println("Troca de dados completa!!!");
										System.out.println("-------------------------------------");
										break;
									case 3:
										System.out.print("Digite um novo endereco: ");
										String endereco = is.nextLine();
										vetEndereco[indice] = endereco;
										System.out.println("-------------------------------------");
										System.out.println("Troca de dados completa!!!");
										System.out.println("-------------------------------------");
										break;
									case 4:
										System.out.print("Digite um novo convenio: ");
										String convenio = is.nextLine();
										vetConvenio[indice] = convenio;
										System.out.println("-------------------------------------");
										System.out.println("Troca de dados completa!!!");
										System.out.println("-------------------------------------");
										break;
									case 5:
										System.out.println("Encerrando troca de dados...");
										System.out.println("-------------------------------------");
										break;
									default:
										System.out.println("Informacao invalida...");
										System.out.println("-------------------------------------");
										break;
								}
							} while (op2 != 5);

						} else {
							System.out.println("Cpf invalido....");
							System.out.println("-------------------------------------");
						}

					} else {
						System.out.println("Nao exitem pacientes cadastrados....");
						System.out.println("-------------------------------------");
					}
					break;
				case 3:
					if (p != 0) {
						int teste25 = 0;
						boolean teste = false;

						System.out.println("Digite as informacoes do paciente ");
						System.out.println("-------------------------------------");

						System.out.print("Nome: ");
						String nome = is.nextLine();

						System.out.print("Idade: ");
						int idade = in.nextInt();

						System.out.print("Convenio: ");
						String convenio = is.nextLine();

						System.out.println("-------------------------------------");

						for (int c = 0; c < p; c++) {
							if (vetNome[c].equalsIgnoreCase(nome) || vetIdade[c] == idade
									|| vetConvenio[c].equalsIgnoreCase(convenio)) {

								if (teste25 == 0) {
									System.out.println("Pacientes encontrados.");
									System.out.println("-------------------------------------");
									teste25++;
								}
								System.out.println("Nome: " + vetNome[c]);
								System.out.println("Cpf: " + vetCpf[c]);
								System.out.println("Idade: " + vetIdade[c]);
								System.out.println("Endereco: " + vetEndereco[c]);
								System.out.println("Convenio: " + vetConvenio[c]);
								System.out.println("-------------------------------------");
								teste = true;

							}
						}
						if (teste == false) {
							System.out.println("Nenhum paciente encontrado... ");
							System.out.println("-------------------------------------");
						}
					} else {
						System.out.println("Nao exitem pacientes cadastrados....");
						System.out.println("-------------------------------------");
					}
					break;
				case 4:
					if (p != 0) {
						boolean teste = false;
						boolean teste2 = false;
						boolean teste3 = false;
						int teste25 = 0;
						int op2;
						int op3;
						Integer vetIndice[] = new Integer[p];

						System.out.print("Digite o nome: ");
						String nome = is.nextLine();

						System.out.print("Digite a idade: ");
						int idade = in.nextInt();
						System.out.println("-------------------------------------");

						for (int c = 0; c < p; c++) {
							if (vetNome[c].equalsIgnoreCase(nome) && vetIdade[c] == idade) {
								if (teste25 == 0) {
									System.out.println("Pacientes compativeis.");
									System.out.println("-------------------------------------");
									teste25++;
								}
								System.out.printf(
										"Nome: %s\n" + "Cpf: %d\n" + "Idade: %d\n" + "Enderco: %s\n" + "Convenio: %s\n"
												+ "Numero para consulta: %d\n",
										vetNome[c], vetCpf[c], vetIdade[c], vetEndereco[c], vetConvenio[c], c);
								vetIndice[c] = c;
								System.out.println("-------------------------------------");
								teste = true;
							}
						}

						if (teste == true) {
							System.out.print("Digite o numero para escolha: ");
							int escolha = in.nextInt();
							System.out.println("-------------------------------------");
							for (int c = 0; c < p; c++) {
								if (vetIndice[c] != null && vetIndice[c] == escolha) {
									teste3 = true;
									for (int cont = 0; cont < p; cont++) {
										if (vetConsultas[cont] != null && vetConsultas[cont] == vetCpf[escolha]) {
											System.out.println("Este paciente ja possui uma consulta marcada...");
											System.out.println("-------------------------------------");
											teste2 = true;
											break;
										}
									}
									if (teste2 == false) {
										do {
											System.out.println("0- Segunda");
											System.out.println("1- Terca");
											System.out.println("2- Quarta");
											System.out.println("3- Quinta");
											System.out.println("4- Sexta");
											System.out.println("5- Sair");
											System.out.println("-------------------------------------");
											System.out.print("Escolha o dia da consulta: ");
											op2 = in.nextInt();
											System.out.println("-------------------------------------");
											if (op2 != 0 && op2 != 1 && op2 != 2 && op2 != 3 && op2 != 4 && op2 != 5) {
												System.out.println("Escolha invalida...");
												System.out.println("-------------------------------------");
											}
										} while (op2 != 0 && op2 != 1 && op2 != 2 && op2 != 3 && op2 != 4 && op2 != 5);
										if (op2 != 5) {
											do {
												System.out.println("0- 8 horas");
												System.out.println("1- 9 horas");
												System.out.println("2- 10 horas");
												System.out.println("3- 11 horas");
												System.out.println("4- 13 horas");
												System.out.println("5- 14 horas");
												System.out.println("6- 15 horas");
												System.out.println("7- 16 horas");
												System.out.println("8- Sair");
												System.out.println("-------------------------------------");
												System.out.print("Escolha um horario: ");
												op3 = in.nextInt();
												System.out.println("-------------------------------------");
												if (op3 != 0 && op3 != 1 && op3 != 2 && op3 != 3 && op3 != 4 && op3 != 5
														&& op3 != 6 && op3 != 7 && op3 != 8) {
													System.out.println("Escolha invalida...");
													System.out.println("-------------------------------------");
												}
											} while (op3 != 0 && op3 != 1 && op3 != 2 && op3 != 3 && op3 != 4
													&& op3 != 5
													&& op3 != 6 && op3 != 7 && op3 != 8);
											if (op3 != 8) {
												vetSemana[op2][op3] = String.format(
														"Nome: %s%n" + "Cpf: %d%n" + "Idade: %d%n" + "Endereco: %s%n"
																+ "Convenio: %s%n" + "Tem consulta na %s as %s horas",
														vetNome[escolha], vetCpf[escolha], vetIdade[escolha],
														vetEndereco[escolha], vetConvenio[escolha], vetDia[op2],
														vetH[op3]);
												vetConsultas[escolha] = vetCpf[escolha];

												System.out.println("Consulta marcada....");
												System.out.println("-------------------------------------");
												break;

											} else {
												System.out.println("Encerrando marcacao de consulta...");
												System.out.println("-------------------------------------");
												break;
											}
										} else {
											System.out.println("Encerrando marcacao de consulta...");
											System.out.println("-------------------------------------");
											break;
										}

									}
								}

							}
							if (teste3 == false) {
								System.out.println("Escolha invalida...");
								System.out.println("-------------------------------------");
							}
						} else {
							System.out.println("Nao existem pacientes com esses dados...");
							System.out.println("-------------------------------------");
						}

					} else {
						System.out.println("Nao exitem pacientes cadastrados....");
						System.out.println("-------------------------------------");
					}
					break;
				case 5:
					try {
						arqRelatorio = new PrintWriter("Pacientes.txt");
						if (p != 0) {
							boolean teste = false;
							for (int c = 0; c < 5; c++) {
								for (int cont = 0; cont < 8; cont++) {
									if (vetSemana[c][cont] != null) {
										if (teste == false) {
											arqRelatorio.println("Pacientes com consulta marcada.");
											arqRelatorio.println(
													"-----------------------------------------------------------------------------------------------------------");
										}
										arqRelatorio.println(vetSemana[c][cont]);
										arqRelatorio.println(
												"-----------------------------------------------------------------------------------------------------------");
										teste = true;
									}
								}
							}
							if (teste == false) {
								arqRelatorio.println("Nenhum paciente com consulta marcada.");
								arqRelatorio.println(
										"-----------------------------------------------------------------------------------------------------------");
							}

						} else {
							arqRelatorio.println("\n não exitem pacientes cadastrados....");
							arqRelatorio.println(
									"-----------------------------------------------------------------------------------------------------------");
						}
						arqRelatorio.flush();
					} catch (FileNotFoundException ex) {
						arqRelatorio.close();
						System.out.println("Erro na saida para arquivo!");
					}
					break;
				case 6:
					System.out.println("Encerrando o programa....");
					System.out.println("-------------------------------------");
					break;
				default:
					System.out.println("Escolha invalida tente novamente");
					System.out.println("-------------------------------------");
					break;

			}
		} while (op != 6);
	}

}