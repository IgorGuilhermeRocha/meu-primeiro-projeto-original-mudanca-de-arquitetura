# Meu primeiro projeto

Este foi o meu primeiro 'projeto' de programação, basicamente um sistema de hospital.
O sistema tem as seguintes funcionalidades:

<ul>
    <li> Cadastro de usuários;
    <li> Busca por pacientes;
    <li> Edição de informações de um usuário;
    <li> Marcar consultas;
    <li> Gerar relatório
</ul>

O projeto foi desenvolvido no começo de 2020, e foi feito utilizando a linguagem java.